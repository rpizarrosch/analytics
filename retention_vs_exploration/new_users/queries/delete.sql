delete from {{params.schema}}.analytics_retention_vs_exploration 
where  1=1
and country = '{{params.country}}'
and new_user = 1
and DAYS_INITIAL_TIME_WINDOW = {{params.bp_window}};