import pandas as pd
import numpy as np
from utils import get_snowflake_data, pre_processing, regression_models
import os

print(os.getcwd())


countries = ["AR", "MX", "CO", "BR", "PE"]

start_month = "2021-01-01"
end_month = "2022-02-02"

import_data_as_csv = False

recent_analysis = start_month == "2022-02-02"

writer = pd.ExcelWriter(
    "./retention_vs_exploration/regression_results/consolidated/recent_allvars_per_scope.xlsx",
    engine="xlsxwriter",
)
writer.save()

writer = pd.ExcelWriter(
    "./retention_vs_exploration/regression_results/consolidated/recent_onevar_per_scope.xlsx",
    engine="xlsxwriter",
)
writer.save()

writer = pd.ExcelWriter(
    "./retention_vs_exploration/regression_results/consolidated/regression_results_parameters.xlsx",
    engine="xlsxwriter",
)
writer.save()

for country in countries:
    writer = pd.ExcelWriter(
        f"./retention_vs_exploration/regression_results/consolidated/final_output_{country.lower()}.xlsx",
        engine="xlsxwriter",
    )
    writer.save()

    writer = pd.ExcelWriter(
        f"./retention_vs_exploration/regression_results/consolidated/recent_onevar_per_scope_{country.lower()}.xlsx",
        engine="xlsxwriter",
    )
    writer.save()

    writer = pd.ExcelWriter(
        f"./retention_vs_exploration/regression_results/consolidated/recent_allvars_per_scope_{country.lower()}.xlsx",
        engine="xlsxwriter",
    )
    writer.save()

df_mean = pd.DataFrame(
    columns=[
        "FIRST_ORDERS_QUANTITY",
        "CATEGORY_FIRST_ORDERS",
        "COUNTRY",
        "USERS",
        "OP_VAR",
        "RECENT",
    ]
)
initial_window = 90

heavy_users_orders = {14: 4, 28: 7, 90: 15}

for start_month in ["2021-03-01", "2022-02-01"]:
    for country in countries:
        recent_analysis = start_month == "2022-02-01"
        print("recent", recent_analysis)

        op_variables = ["AVG_BES_V12_HISTORICAL"]

        users_scope = ["ALL_USERS", "NEW_USERS", "NON_NEW_USERS", "HEAVY_USERS"]

        numerical_user_features = [
            "ACQUISITION_SCORE",
            "RETENTION_SCORE",
            "REACTIVATION_SCORE",
            "AGE_IN_RAPPI_IN_MONTHS",
            "DISTINCT_CATEGORIES_BEFORE_BP",
        ]

        invalid_values_numerical_user_features = [
            "ACQUISITION_SCORE",
            "RETENTION_SCORE",
            "REACTIVATION_SCORE",
        ]

        categorical_columns = [
            "COUNTRY",
            "CITY_NAME",
            "FIRST_BRAND_CATEGORY",
            "SEGMENT_RFM",
            "CATEGORY",
            "ULTRA_SCORE",
            "MONTH",
            "FIRST_ORDER_MONTH",
        ]

        if import_data_as_csv:
            df_query = pd.read_csv(
                f"./retention_vs_exploration/data/{country}_start_month_{start_month}_end_month_{end_month}.csv",
                sep=";",
            )
            df_nulls = pd.read_csv(
                f"./retention_vs_exploration/data/{country}_start_month_{start_month}_end_month_{end_month}_NULLS.csv",
                sep=";",
            )
        else:

            query = f"""
            with aux as (
            select *,
            month(first_order_month::date) as month,
            row_number() over (partition by application_user_id,days_initial_time_window order by first_order_month::date asc) as rn
            from PE_WRITABLE.analytics_retention_vs_exploration
            where 1=1
            and (first_order_month between '{start_month}' and '{end_month}' )
            and country = '{country}'
            and DAYS_INITIAL_TIME_WINDOW = {initial_window}
            qualify rn= 1
            )

            select *
            from aux sample (1000000 rows)

            """

            snowflake_connector = get_snowflake_data.SnowflakeConnector()
            df_query, df_nulls = snowflake_connector.get_pandas_dataframe_from_query(
                query, fillna_0=False, get_null_df=True
            )
            df_query.to_csv(
                f"./retention_vs_exploration/data/{country}_start_month_{start_month}_end_month_{end_month}.csv",
                index=False,
                sep=";",
            )
            df_nulls.to_csv(
                f"./retention_vs_exploration/data/{country}_start_month_{start_month}_end_month_{end_month}_NULLS.csv",
                index=False,
                sep=";",
            )
            print("Query Done")
        print("Shape post query", df_query.shape)

        df_query = pre_processing.get_dummies_by_rank_based_in_quantile(
            df_query,
            columns_to_dummie=["FIRST_MICROZONE"],
            columns_to_count=["APPLICATION_USER_ID"],
            quantile=10,
            duplicates="drop",
        )
        print("Shape post get_dummies_by_rank_based_in_quantile", df_query.shape)

        df_query["PCT_FIRST_ORDERS"] = df_query["FIRST_ORDERS_QUANTITY"] / df_query["QUANT_ORDERS"]
        df_query["PCT_FIRST_ORDERS"] = df_query["PCT_FIRST_ORDERS"].fillna(0)

        # df_query["AVG_ERROR_ETA_EARLY"] = df_query["AVG_ETA_FROM_START"] - df_query["AVG_ORDER_TIME"]
        # df_query["AVG_ERROR_ETA_LATE"] = df_query["AVG_ORDER_TIME"] - df_query["AVG_ETA_FROM_START"]

        # df_query.loc[df_query.loc[:, "AVG_ERROR_ETA_EARLY"] < 0, "AVG_ERROR_ETA_EARLY"] = 0

        # df_query.loc[df_query.loc[:, "AVG_ERROR_ETA_LATE"] < 0, "AVG_ERROR_ETA_LATE"] = 0

        # Conversion de EQS
        for col in df_query.columns:
            if "EQS" in col:
                df_query[col] = np.abs((100 / 170) * df_query[col])
        df_nulls = df_nulls[df_nulls["COLUMN"] != "ORDERS_L365_DAYS"]

        # Columnas con flag en variables con nulos
        df_query = pre_processing.boolean_on_nan_columns(df_query, df_nulls, fillna_0=False)

        # Dropeo variables con alta dimensionalidad (no me da la RAM)
        drop_categoricals = ["COUNTRY", "FIRST_ORDER_MONTH"]

        df_categorical_dummies = pre_processing.dummies_and_concat(df_query, categorical_columns, drop_categoricals)
        print("Shape post dummies_and_concat", df_categorical_dummies.shape)

        # Add categorical dummmies
        categorical_vars = []
        for var in categorical_columns:
            for col in df_categorical_dummies.columns:
                if var in col:
                    categorical_vars.append(col)

        regression_variables_list = [
            "QUANT_ORDERS",
            "POST_ORDERS",
            "ORDERS_L365_DAYS",
            "NEW_USER",
            "CATEGORY_FIRST_ORDERS",
            "FIRST_ORDERS_QUANTITY",
            "COMPENSATION_RATE",
            "DISTINCT_CATEGORIES_BEFORE_BP",
        ]

        regression_variables_list.extend(categorical_vars)

        regression_variables_list.extend(numerical_user_features)

        variables_to_log = [
            "POST_ORDERS",
        ]

        pct_variables = [
            # 'USER_CANCEL_RATE',
            # 'AVG_STORE_DEFECT_RATE',
            # 'AVG_STORE_CANCEL_RATE',
            "COMPENSATION_RATE"
        ]
        print("Shape before pct_variables", df_categorical_dummies.shape)
        for pct_var in pct_variables:
            df_categorical_dummies = df_categorical_dummies.loc[
                (df_categorical_dummies[pct_var] <= 1) & (df_categorical_dummies[pct_var] >= 0),
                :,
            ]
            df_categorical_dummies.loc[:, pct_var] = df_categorical_dummies.loc[:, pct_var] * 100
        print("Shape post pct_variables", df_categorical_dummies.shape)

        for var in invalid_values_numerical_user_features:
            df_categorical_dummies = df_categorical_dummies.loc[df_categorical_dummies[var] > 0, :]
        print(
            "Shape post invalid_values_numerical_user_features",
            df_categorical_dummies.shape,
        )

        for users in users_scope:
            print(country, users)

            # Defino Scope
            if users == "ALL_USERS":
                df = df_categorical_dummies
            if users == "NEW_USERS":
                df = df_categorical_dummies
                df = df[df["NEW_USER"] == 1]
                df.fillna({"NUM_APP_LAUNCH_LST_30_D": 0})
            if users == "NON_NEW_USERS":
                df = df_categorical_dummies
                df = df[df["NEW_USER"] == 0]

            if users == "HEAVY_USERS":
                df = df_categorical_dummies
                df = df[df["QUANT_ORDERS"] >= heavy_users_orders[initial_window]]
            print("Shape post scope", df.shape)
            with open("retention_vs_exploration/logs.txt", mode="a") as f:
                f.write(f"{country} {users} Shape post scope {df.shape} \n")
            # Outliers y errores en datos
            df = pre_processing.delete_outliers_by_percentiles(
                df,
                columns_with_outliers=[
                    "QUANT_ORDERS",
                    "POST_ORDERS",
                    "COMPENSATION_RATE",
                    "CATEGORY_FIRST_ORDERS",
                    "FIRST_ORDERS_QUANTITY",
                    "DISTINCT_CATEGORIES_BEFORE_BP",
                ],
                percentile=0.99,
            )
            print("Shape post delete_outliers_by_percentiles", df.shape)

            df["ORDERS_L365_DAYS"].fillna(0, inplace=True)

            # For para cada variable que queremos ver
            for op_var in op_variables:
                regression_variables = regression_variables_list.copy()
                print(country, users, op_var)

                regression_variables.insert(0, op_var)
                variables_to_log.insert(0, op_var)

                # initialize a null list
                unique_list = []

                for x in regression_variables:
                    # check if exists in unique_list or not
                    if x not in unique_list:
                        unique_list.append(x)
                regression_variables = unique_list

                df_regression = df[regression_variables].fillna(
                    {"FIRST_ORDERS_QUANTITY": 0, "DISTINCT_CATEGORIES_BEFORE_BP": 0}
                )
                for reg_var in regression_variables:
                    df_regression[reg_var] = pd.to_numeric(df_regression[reg_var], errors="coerce")
                    if reg_var == "AVG_BAD_EXPERIENCE_SCORE":
                        df_regression.loc[:, reg_var] = df_regression.loc[:, reg_var].apply(np.abs)
                        df_regression[reg_var] = pd.to_numeric(
                            df_regression[reg_var], errors="coerce", downcast="integer"
                        )

                nulls = df_regression.isna().sum()
                quant = nulls / len(df_regression)
                df_nulls = pd.DataFrame(
                    {
                        "COLUMN": nulls.index,
                        "AMOUNT_OF_NULLS": nulls,
                        "PERC_OF_NULLS": quant,
                    }
                )
                df_nulls = df_nulls[df_nulls["AMOUNT_OF_NULLS"] > 0].reset_index(drop=True)
                df_nulls.sort_values(by="PERC_OF_NULLS", ascending=False, inplace=True)
                print("Amount of Nulls:\n", df_nulls.head(5))
                print("Shape pere replace nan", df_regression.shape)
                df_regression.replace([np.inf, -np.inf], np.nan, inplace=True)
                df_regression.dropna(inplace=True)
                print("Shape post dropna:", df_regression.shape)
                with open("retention_vs_exploration/logs.txt", mode="a") as f:
                    f.write(f"{country} {users} Shape post dropna {df.shape} \n")

                df_mean_aux = df_regression[["FIRST_ORDERS_QUANTITY", "CATEGORY_FIRST_ORDERS"]].mean()
                df_mean_aux["COUNTRY"] = country
                df_mean_aux["USERS"] = users
                df_mean_aux["OP_VAR"] = op_var
                df_mean_aux["RECENT"] = recent_analysis
                df_mean = df_mean.append(df_mean_aux, ignore_index=True)
                df_regression.to_csv(
                    "./retention_vs_exploration/data/df_regression_pre_logs.csv",
                    index=False,
                )
                reg_model = regression_models.RegressionModel(df_regression=df_regression, target_var="POST_ORDERS")
                # Apply log to columns defined in variables_to_log
                reg_model.apply_log(variables_to_log)

                # Fit regression
                fitted_model = reg_model.fit_regression()
                regression_results = reg_model.get_results_dataframe(readability=False)

                variables_to_log.remove(op_var)
                regression_variables.remove(op_var)

                if recent_analysis:
                    with pd.ExcelWriter(
                        f"./retention_vs_exploration/regression_results/consolidated/recent_onevar_per_scope_{country.lower()}.xlsx",
                        mode="a",
                        if_sheet_exists="replace",
                        engine="openpyxl",
                    ) as writer:
                        regression_results.to_excel(writer, sheet_name=f"{country}_{users}_{op_var}")

                else:
                    with pd.ExcelWriter(
                        f"./retention_vs_exploration/regression_results/consolidated/final_output_{country.lower()}.xlsx",
                        mode="a",
                        if_sheet_exists="replace",
                        engine="openpyxl",
                    ) as writer:
                        regression_results.to_excel(writer, sheet_name=f"{country}_{users}_{op_var}")

            # All op vars in same regression
            # print("All Vars regression ", "-", users)
            # del regression_variables
            # regression_variables = op_variables.copy()
            # aux = regression_variables_list.copy()
            # regression_variables.extend(aux)
            # # initialize a null list
            # unique_list = []
            # for x in regression_variables:
            #     # check if exists in unique_list or not
            #     if x not in unique_list:
            #         unique_list.append(x)
            # regression_variables = unique_list

            # df_regression = df[regression_variables].fillna(
            #     {"FIRST_ORDERS_QUANTITY": 0, "DISTINCT_CATEGORIES_BEFORE_BP": 0}
            # )
            # for reg_var in regression_variables:
            #     df_regression[reg_var] = pd.to_numeric(
            #         df_regression[reg_var], errors="coerce"
            #     )

            # df_regression.dropna(inplace=True)
            # df_mean_aux = df_regression[
            #     ["FIRST_ORDERS_QUANTITY", "CATEGORY_FIRST_ORDERS"]
            # ].mean()
            # df_mean_aux["COUNTRY"] = country
            # df_mean_aux["USERS"] = users
            # df_mean_aux["OP_VAR"] = "ALL_VARS"
            # df_mean_aux["RECENT"] = recent_analysis
            # df_mean = df_mean.append(df_mean_aux, ignore_index=True)
            # reg_model = regression_models.RegressionModel(
            #     df_regression=df_regression.dropna(), target_var="POST_ORDERS"
            # )
            # # Apply log to columns defined in variables_to_log
            # log_aux = op_variables.copy()
            # log_aux.extend(variables_to_log)

            # reg_model.apply_log(log_aux)

            # # Fit regression
            # fitted_model = reg_model.fit_regression()

            # regression_results = reg_model.get_results_dataframe(readability=False)

            # if recent_analysis:
            #     with pd.ExcelWriter(
            #         f"./retention_vs_exploration/regression_results/consolidated/recent_allvars_per_scope_{country.lower()}.xlsx",
            #         mode="a",
            #         if_sheet_exists="replace",
            #         engine="openpyxl",
            #     ) as writer:
            #         regression_results.to_excel(
            #             writer, sheet_name=f"{country}_{users}_ALL_VARS"
            #         )
            # else:
            #     with pd.ExcelWriter(
            #         "./retention_vs_exploration/regression_results/consolidated/final_output_all_vars.xlsx",
            #         mode="a",
            #         if_sheet_exists="replace",
            #         engine="openpyxl",
            #     ) as writer:
            #         regression_results.to_excel(
            #             writer, sheet_name=f"{country}_{users}_ALL_VARS"
            #         )

df_mean.to_csv(
    "./retention_vs_exploration/regression_results/consolidated/variables_mean.csv",
    index=False,
    sep=";",
)
