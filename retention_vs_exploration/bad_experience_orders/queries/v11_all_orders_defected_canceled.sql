BEGIN;

DELETE FROM {{ params.schema }}.users_bad_experiences_analytics
where 1=1
 AND VERSION = 'V11_ALL_ORDERS_CANCELED_DEFECTED'
 AND COUNTRY = '{{ params.country }}';

insert into {{ params.schema }}.users_bad_experiences_analytics

with 
ORDERS_BASE_WITHOUT_FILTERS as (
    SELECT 
        ORDER_ID,
        USER_ID,
        CREATED_AT,
        BRAND_ID,
        N_ORDER,
        NEXT_ORDER_ID,
        REPURCHASE_BRAND,
        LAST_ORDER_FLAG
    from (
        select o.order_id
            , o.application_user_id user_id
            , o.created_at
            , o.brand_id
            , row_number() over(partition by o.brand_id, o.application_user_id order by o.created_at) n_order
            , lead(o.order_id) over(partition by o.brand_id, o.application_user_id order by o.created_at) next_order_id
            , case when next_order_id is not null then 1 else 0 end repurchase_brand
            , CASE WHEN REPURCHASE_BRAND = 0 THEN 1 ELSE 0 END AS LAST_ORDER_FLAG
        from global_finances.{{ params.country }}_orders o
        where o.store_type = 'restaurant'
            and o.order_state not in (
                'canceled_for_payment_error'
                , 'canceled_by_fraud'
                , 'canceled_by_split_error'
            )
    ) t
    where 1=1
        and t.created_at::date >= current_date - interval '{{ params.interval_time }}'
    qualify n_order <= 3
)

, BRANDS_WITHOUT_REPURCHASE AS (
    SELECT DISTINCT 
        USER_ID,
        BRAND_ID,
        DATEDIFF('day',CREATED_AT,CURRENT_DATE) DAYS_SINCE_LAST_ORDER
    FROM ORDERS_BASE_WITHOUT_FILTERS
    WHERE 1=1 
        AND LAST_ORDER_FLAG = 1
        AND CREATED_AT::DATE < '2022-02-01'::DATE
)

, ORDERS_BASE AS (
    SELECT 
    DISTINCT 
        O.ORDER_ID,
        O.USER_ID,
        O.CREATED_AT,
        O.BRAND_ID,
        O.N_ORDER,
        O.NEXT_ORDER_ID,
        O.REPURCHASE_BRAND,
        O.LAST_ORDER_FLAG
    FROM ORDERS_BASE_WITHOUT_FILTERS O
    INNER JOIN BRANDS_WITHOUT_REPURCHASE BWR ON 1=1
                                                AND O.USER_ID = BWR.USER_ID
                                                AND O.BRAND_ID = BWR.BRAND_ID

)

, defected as(
    select distinct t.order_id
    from ops_global.non_live_tickets_details_v2 t
    where         level_2 in (
            'Missing product',
            'different product',
            'Product in poor condition'
        )
        and upper(t.country) = '{{ params.country }}'
        and kustomer_created_at::date >= current_date - interval '{{ params.interval_time }}'
)
, cancelled_orders as (
    select order_id
    from OPS_GLOBAL.CANCELLATION_REASONS
    where upper(country) = '{{ params.country }}'
        and state not in ('canceled_by_early_regret')
        and vertical = 'Restaurantes'
        and level_1 = 'partner_related_errors'
        and created_at::date >= current_date - interval '{{ params.interval_time }}'
)

, orders_info as (
    select distinct
         o.user_id
         , o.brand_id
         , o.order_id
         , o.created_at
        , case when d.order_id is not null then 1 else 0 end defected
        , case when c.order_id is not null then 1 else 0 end canceled
    from orders_base o
        left join defected d on
            o.order_id = d.order_id
        left join cancelled_orders c on
            o.order_id = c.order_id
)



select o.user_id
    , '{{ params.country }}' country
    , o.brand_id
    , o.order_id
    , o.created_at
    , current_timestamp updated_at
    , 'V11_ALL_ORDERS_CANCELED_DEFECTED' AS VERSION
from orders_info o
where (o.canceled = 1
    or o.defected = 1
);


COMMIT;