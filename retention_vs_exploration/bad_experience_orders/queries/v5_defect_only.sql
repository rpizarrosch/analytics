BEGIN;

DELETE FROM {{ params.schema }}.users_bad_experiences_analytics
where 1=1
 AND VERSION = 'V5_DEFECTS_ONLY'
 AND COUNTRY = '{{ params.country }}';


insert into {{ params.schema }}.users_bad_experiences_analytics

with 
orders_base as (
    SELECT 
        ORDER_ID,
        USER_ID,
        CREATED_AT,
        BRAND_ID,
        N_ORDER,
        NEXT_ORDER_ID,
        REPURCHASE_BRAND,
        LAST_ORDER_FLAG
    from (
        select o.order_id
            , o.application_user_id user_id
            , o.created_at
            , o.brand_id
            , row_number() over(partition by o.brand_id, o.application_user_id order by o.created_at) n_order
            , lead(o.order_id) over(partition by o.brand_id, o.application_user_id order by o.created_at) next_order_id
            , case when next_order_id is not null then 1 else 0 end repurchase_brand
            , CASE WHEN REPURCHASE_BRAND = 0 THEN 1 ELSE 0 END AS LAST_ORDER_FLAG
        from global_finances.{{ params.country }}_orders o
        where o.store_type = 'restaurant'
            and o.order_state not in (
                'canceled_for_payment_error'
                , 'canceled_by_fraud'
                , 'canceled_by_split_error'
            )
    ) t
    where t.n_order in (1, 2, 3)
        and t.created_at::date >= current_date - interval '{{ params.interval_time }}'
        and t.repurchase_brand = 0
)
, defected as(
    select distinct t.order_id
    from ops_global.non_live_tickets_details_v2 t
    where         level_2 in (
            'Missing product',
            'different product',
            'Product in poor condition'
        )
        and upper(t.country) = '{{ params.country }}'
        and kustomer_created_at::date >= current_date - interval '{{ params.interval_time }}'
)

, orders_info as (
    select distinct
         o.user_id
         , o.brand_id
         , o.order_id
         , o.created_at
        , case when d.order_id is not null then 1 else 0 end defected
    from orders_base o
        left join defected d on
            o.order_id = d.order_id
)

select o.user_id
    , '{{ params.country }}' country
    , o.brand_id
    , o.order_id
    , o.created_at
    , current_timestamp updated_at
    , 'V5_DEFECTS_ONLY' AS VERSION
from orders_info o
where 1=1
    AND o.defected = 1
;


COMMIT;