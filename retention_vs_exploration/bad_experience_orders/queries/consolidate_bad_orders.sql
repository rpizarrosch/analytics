
DELETE FROM {{params.schema}}.USERS_BAD_EXPERIENCES_CONSOLIDATED
WHERE COUNTRY = '{{params.country}}';


INSERT INTO {{params.schema}}.USERS_BAD_EXPERIENCES_CONSOLIDATED

WITH ORDERS_REPEATED AS (
    SELECT 
        USER_ID,
        COUNTRY,
        BRAND_ID,
        ORDER_ID,
        CREATED_AT::DATE AS CREATED_AT,
        UPDATED_AT::DATE AS UPDATED_AT,
        CASE WHEN VERSION = 'V1_FIRST_3_ORDERS' THEN 1 ELSE 0 END AS V1_FIRST_3_ORDERS,
        CASE WHEN VERSION = 'V2_ALL_ORDERS_NO_REPURCHASE' THEN 1 ELSE 0 END AS V2_ALL_ORDERS_NO_REPURCHASE,
        CASE WHEN VERSION = 'V3_ALL_ORDERS_WITH_REPURCHASE' THEN 1 ELSE 0 END AS V3_ALL_ORDERS_WITH_REPURCHASE,
        CASE WHEN VERSION = 'V4_BAD_RATINGS_ONLY' THEN 1 ELSE 0 END AS V4_BAD_RATINGS_ONLY,
        CASE WHEN VERSION = 'V5_DEFECTS_ONLY' THEN 1 ELSE 0 END AS V5_DEFECTS_ONLY,
        CASE WHEN VERSION = 'V6_DELAY_ONLY' THEN 1 ELSE 0 END AS V6_DELAY_ONLY,
        CASE WHEN VERSION = 'V7_CANCELED_ONLY' THEN 1 ELSE 0 END AS V7_CANCELED_ONLY,
        CASE WHEN VERSION = 'V8_DEFECTED_CANCELED' THEN 1 ELSE 0 END AS V8_DEFECTED_CANCELED,
        CASE WHEN VERSION = 'V9_DEFECTED_CANCELED_BAD_REVIEW' THEN 1 ELSE 0 END AS V9_DEFECTED_CANCELED_BAD_REVIEW,
        CASE WHEN VERSION = 'V10_ALL_ORDERS_NO_REPURCHASE' THEN 1 ELSE 0 END AS V10_ALL_ORDERS_NO_REPURCHASE,
        CASE WHEN VERSION = 'V11_ALL_ORDERS_CANCELED_DEFECTED' THEN 1 ELSE 0 END AS V11_ALL_ORDERS_CANCELED_DEFECTED
    FROM {{params.schema}}.USERS_BAD_EXPERIENCES_ANALYTICS
        WHERE 1=1
            AND COUNTRY = '{{params.country}}'
)

, UNIQUE_ORDERS AS (
    SELECT 
        USER_ID,
        COUNTRY,
        BRAND_ID,
        ORDER_ID,
        CREATED_AT,
        UPDATED_AT,
        MAX(V1_FIRST_3_ORDERS) AS V1_FIRST_3_ORDERS,
        MAX(V2_ALL_ORDERS_NO_REPURCHASE) AS V2_ALL_ORDERS_NO_REPURCHASE,
        MAX(V3_ALL_ORDERS_WITH_REPURCHASE) AS V3_ALL_ORDERS_WITH_REPURCHASE,
        MAX(V4_BAD_RATINGS_ONLY) AS V4_BAD_RATINGS_ONLY,
        MAX(V5_DEFECTS_ONLY) AS V5_DEFECTS_ONLY,
        MAX(V6_DELAY_ONLY) AS V6_DELAY_ONLY,
        MAX(V7_CANCELED_ONLY) AS V7_CANCELED_ONLY,
        MAX(V8_DEFECTED_CANCELED) AS V8_DEFECTED_CANCELED,
        MAX(V9_DEFECTED_CANCELED_BAD_REVIEW) AS V9_DEFECTED_CANCELED_BAD_REVIEW,
        MAX(V10_ALL_ORDERS_NO_REPURCHASE) AS V10_ALL_ORDERS_NO_REPURCHASE,
        MAX(V11_ALL_ORDERS_CANCELED_DEFECTED) AS V11_ALL_ORDERS_CANCELED_DEFECTED
         FROM ORDERS_REPEATED
        GROUP BY         
        USER_ID,
        COUNTRY,
        BRAND_ID,
        ORDER_ID,
        CREATED_AT,
        UPDATED_AT
)

SELECT         
    USER_ID,
    COUNTRY,
    BRAND_ID,
    ORDER_ID,
    CREATED_AT,
    UPDATED_AT,
    V1_FIRST_3_ORDERS,
    V2_ALL_ORDERS_NO_REPURCHASE,
    V3_ALL_ORDERS_WITH_REPURCHASE,
    V4_BAD_RATINGS_ONLY,
    V5_DEFECTS_ONLY,
    V6_DELAY_ONLY,
    V7_CANCELED_ONLY,
    V8_DEFECTED_CANCELED,
    V9_DEFECTED_CANCELED_BAD_REVIEW,
    V10_ALL_ORDERS_NO_REPURCHASE,
    V11_ALL_ORDERS_CANCELED_DEFECTED
FROM UNIQUE_ORDERS;