BEGIN;

DELETE FROM {{ params.schema }}.users_bad_experiences_analytics
where 1=1
 AND VERSION = 'V9_DEFECTED_CANCELED_BAD_REVIEW'
 AND COUNTRY = '{{ params.country }}';


insert into {{ params.schema }}.users_bad_experiences_analytics

with 
orders_base as (
    SELECT 
        ORDER_ID,
        USER_ID,
        CREATED_AT,
        BRAND_ID,
        N_ORDER,
        NEXT_ORDER_ID,
        REPURCHASE_BRAND,
        LAST_ORDER_FLAG
    from (
        select o.order_id
            , o.application_user_id user_id
            , o.created_at
            , o.brand_id
            , row_number() over(partition by o.brand_id, o.application_user_id order by o.created_at) n_order
            , lead(o.order_id) over(partition by o.brand_id, o.application_user_id order by o.created_at) next_order_id
            , case when next_order_id is not null then 1 else 0 end repurchase_brand
            , CASE WHEN REPURCHASE_BRAND = 0 THEN 1 ELSE 0 END AS LAST_ORDER_FLAG
        from global_finances.{{ params.country }}_orders o
        where o.store_type = 'restaurant'
            and o.order_state not in (
                'canceled_for_payment_error'
                , 'canceled_by_fraud'
                , 'canceled_by_split_error'
            )
    ) t
    where t.n_order in (1, 2, 3)
        and t.created_at::date >= current_date - interval '{{ params.interval_time }}'
        and t.repurchase_brand = 0
)
, defected as(
    select distinct t.order_id
    from ops_global.non_live_tickets_details_v2 t
    where         level_2 in (
            'Missing product',
            'different product',
            'Product in poor condition'
        )
        and upper(t.country) = '{{ params.country }}'
        and kustomer_created_at::date >= current_date - interval '{{ params.interval_time }}'
)
, cancelled_orders as (
    select order_id
    from OPS_GLOBAL.CANCELLATION_REASONS
    where upper(country) = '{{ params.country }}'
        and state not in ('canceled_by_early_regret')
        and vertical = 'Restaurantes'
        and level_1 = 'partner_related_errors'
        and created_at::date >= current_date - interval '{{ params.interval_time }}'
)

, bad_reviews as (
    select r.order_id 
        , max(score) score
    from (
        select r.order_id
            , a.score
        from {{ params.country }}_pg_ms_support_ratings_public.ratings r
        join {{ params.country }}_pg_ms_support_ratings_public.answers a on 
            r.id = a.rating_id
            and not nvl(a._fivetran_deleted, false)
        join {{ params.country }}_pg_ms_support_ratings_public.ratings_user_id ru on 
            a.id = ru.answer_id
            and not nvl(ru._fivetran_deleted, false)
            and ru.actor_type = 'partner'
        where r.receiver_type = 'client'
            and r.deleted_at is null
            and not nvl(r._fivetran_deleted, false)
            and r.type in ('SUPPORT_RATING_CLIENT_FEEDBACK', 'RATE_AND_REVIEW_STARS')
            and r.created_at::date >= current_date - interval '{{ params.interval_time }}'
        
        union all
        
        select r.order_id
                , r.score
        from {{ params.country }}_pg_ms_partners_growth_public.reviews r
        where not nvl(r._fivetran_deleted, false)
            and r.created_at::date >= current_date - interval '{{ params.interval_time }}'

    ) r
    group by r.order_id
    having  max(score) in (1, 2, 3)
)

, orders_info as (
    select distinct
         o.user_id
         , o.brand_id
         , o.order_id
         , o.created_at
        , case when d.order_id is not null then 1 else 0 end defected
        , case when c.order_id is not null then 1 else 0 end canceled
        , case when b.order_id is not null then 1 else 0 end bad_review
    from orders_base o
        left join defected d on
            o.order_id = d.order_id
        left join cancelled_orders c on
            o.order_id = c.order_id
        left join bad_reviews b on
            o.order_id = b.order_id
)

select o.user_id
    , '{{ params.country }}' country
    , o.brand_id
    , o.order_id
    , o.created_at
    , current_timestamp updated_at
    , 'V9_DEFECTED_CANCELED_BAD_REVIEW' AS VERSION
from orders_info o
where 1=1
    AND (o.defected = 1 OR o.canceled =1 or o.bad_review = 1)

;


COMMIT;