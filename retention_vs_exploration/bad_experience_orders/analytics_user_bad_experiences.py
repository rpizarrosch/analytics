"""DAG to calculate bad experiences versions."""
from airflow import DAG
from dags.resto_ds import settings
from datetime import datetime
import os
from plugins.operators.snowflake_operator import MultiStatementSnowflakeOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.task_group import TaskGroup
from airflow.operators.trigger_dagrun import TriggerDagRunOperator


# DAG Setup
queries_base_path = os.path.join(os.path.dirname(__file__), "queries")

default_args = settings.get_default_args(owners=["rodrigo.pizarro@rappi.com"])


# Environment specific variables
if os.environ["AIRFLOW_ENVIRONMENT"] == "production":
    schema = "events_home_restaurants"
    stage_name = "events_home_restaurants.resto_ds_stage_prod"
    countries = ["MX", "CO", "BR", "AR", "PE"]
    interval_time = "26 month"
else:
    schema = "ar_writable"
    stage_name = "ubs_stage_test"
    countries = ["MX", "CO", "BR", "AR", "PE"]
    interval_time = "1 month"  # Since june 2020 to have 6 months prior to january of 2021.


with DAG(
    "analytics_bad_experience_orders",
    catchup=False,
    default_args=default_args,
    schedule_interval=None,
    max_active_runs=1,
    template_searchpath=queries_base_path,
    start_date=datetime(2022, 2, 23),
) as dag:

    create_table = MultiStatementSnowflakeOperator(
        task_id="create_table", sql="create_table.sql", params={"schema": schema}
    )
    trigger_reduce_input_interactions = TriggerDagRunOperator(
        task_id="trigger_analytics_new_users",
        trigger_dag_id="analytics_retention_vs_exploration_nus",
        execution_date="{{ ds }}",
        reset_dag_run=True,
    )

    end_op = DummyOperator(task_id="start_consolidate", dag=dag)

    with TaskGroup(group_id="v8_defect_canceled") as v8_countries:
        for country in countries:
            v8 = MultiStatementSnowflakeOperator(
                task_id=f"{country}_v8_defect_canceled",
                sql="v8_defect_canceled.sql",
                params={"country": country, "schema": schema, "interval_time": interval_time},
            )

    with TaskGroup(group_id="consolidate_bad_orders") as consolidate_orders:
        for country in countries:
            consolidate = MultiStatementSnowflakeOperator(
                task_id=f"{country}_consolidate_bad_orders",
                sql="consolidate_bad_orders.sql",
                params={"schema": schema, "country": country},
            )

(
    create_table
    >> [
        v8_countries,
    ]
    >> end_op
    >> consolidate_orders
    >> trigger_reduce_input_interactions
)
