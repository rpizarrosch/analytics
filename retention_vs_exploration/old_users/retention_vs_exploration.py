"""DAG to calculate retention vs exploration analysis."""

from airflow import DAG
from dags.resto_ds import settings
from datetime import datetime
import os
from operators.snowflake_operator import MultiStatementSnowflakeOperator
from airflow.operators.dummy import DummyOperator
from airflow.utils.task_group import TaskGroup

# DAG Setup
queries_base_path = os.path.join(os.path.dirname(__file__), "queries")

default_args = settings.get_default_args(owners=["rodrigo.pizarro@rappi.com"])


# Environment specific variables
if os.environ["AIRFLOW_ENVIRONMENT"] == "production":
    schema = "events_home_restaurants"
    stage_name = "events_home_restaurants.resto_ds_stage_prod"
    countries = ["MX", "CO", "BR", "AR", "PE"]

else:
    schema = "cl_writable"
    stage_name = "ubs_stage_test"
    countries = ["MX", "CO", "BR", "AR", "PE"]


month_start = [
    "2021-03-01",
    "2021-04-01",
    "2021-05-01",
    "2021-06-01",
    "2021-07-01",
    "2021-08-01",
    "2021-09-01",
    "2021-10-01",
    "2021-11-01",
    "2021-12-01",
    "2022-01-01",
    "2022-02-02",
]

bp_windows = [14, 28, 90]
ep_windows = [14, 14, 90]
get_logs = False


with DAG(
    "analytics_retention_vs_exploration_old_users",
    catchup=False,
    default_args=default_args,
    schedule_interval=None,
    max_active_runs=1,
    template_searchpath=queries_base_path,
    start_date=datetime(2022, 2, 23),
) as dag:
    # List of task
    start_op = DummyOperator(task_id="start_task", dag=dag)

    end_op = DummyOperator(task_id="end_task", dag=dag)
    for country in countries:
        delete_country = cohorts_t3 = MultiStatementSnowflakeOperator(
            task_id=f"{country}_delete",
            sql="delete.sql",
            params={"country": country, "schema": schema, "bp_window": bp_windows[2]},
        )
        country_tasks = DummyOperator(task_id=f"{country}_tasks", dag=dag)
        with TaskGroup(group_id=f"{country}_months") as country_months:

            for month in month_start:
                cohorts_t3 = MultiStatementSnowflakeOperator(
                    task_id=f"{country}_{month}_cohorts_t1_{bp_windows[2]}",
                    sql="retention_cohorts_by_exploration_level.sql",
                    params={
                        "country": country,
                        "schema": schema,
                        "month": month,
                        "min_orders": 3,
                        "bp_window": bp_windows[2],
                        "ep_window": ep_windows[2],
                    },
                )

        (start_op >> country_tasks >> delete_country >> country_months >> end_op)
