insert into {{params.schema}}.analytics_retention_vs_exploration


with
  users_bad_experiences as (
  select distinct user_id,order_id,country
  from events_home_restaurants.users_bad_experiences
  where 1=1
  and country = '{{params.country}}'
  and created_at::date >= '{{params.month}}'
  and created_at::date <= dateadd(month,4,'{{params.month}}')
)
--Microzones - City
, microzone_city as (
  select  mz.id microzone_id,
        mz.NAME microzone,
        ca.CITY,
        mz.city_id
  from {{params.country}}_GRABILITY_PUBLIC.MICRO_ZONES_VW mz
  left join {{params.country}}_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses ca on ca.id = mz.city_id  
  where
   nvl(mz._FIVETRAN_DELETED,false) = false

)
-- New users by country
-- Get new users from the previous calculations.
, new_users_first_orders as (
    select
        distinct application_user_id
        from ar_writable.analytics_retention_vs_exploration
        where country = '{{params.country}}' and new_user=1
)

-- Users of this month
, this_month_users as 
(
  select  o.application_user_id,
  min(o.created_at::date) first_order_date
  from global_finances.{{params.country}}_orders o
  left join new_users_first_orders on o.application_user_id = new_users_first_orders.application_user_id
  where 1=1
  and 
  ( 
  (o.created_at::date between '{{params.month}}' and DATEADD(day,-1,DATEADD(month,1,'{{params.month}}')))

  )
  and state_type = 'FINISHED'
  and vertical_group = 'RESTAURANTS'
  and new_users_first_orders.application_user_id is null
  group by 1
  

)


, total_3_fo as (

  select brand_id,
          application_user_id,
          order_id,
          row_number() over(partition by o.brand_id, o.application_user_id order by o.created_at) n_order
  from global_finances.{{params.country}}_orders o
  where 1=1
    and o.order_state not in 
    ('canceled_for_payment_error', 
    'canceled_by_fraud', 
    'canceled_by_split_error',
    'canceled_by_early_regret')
    and o.vertical_group = 'RESTAURANTS'
  qualify n_order <= 3


)


, bad_experience_score as (
 with orders as (
    select 
      o.created_at::date as created_at,
      o.order_id,
      o.application_user_id,
      o.store_id,
      o.microzone_id,
      CASE 
      WHEN TIME(o.created_at) BETWEEN '05:00:00' AND '11:00:00' THEN 'morning'
      WHEN TIME(o.created_at) BETWEEN '11:00:01' AND '15:00:00' THEN 'noon'
      WHEN TIME(o.created_at) BETWEEN '15:00:01' AND '18:00:00' THEN 'afternoon'
      ELSE 'night' END order_moment
    from global_finances.{{params.country}}_orders o
    inner join total_3_fo on o.order_id = total_3_fo.order_id
    inner join this_month_users on o.application_user_id = this_month_users.application_user_id
    where 1=1 
    AND o.created_at BETWEEN this_month_users.first_order_date  and DATEADD(day,{{params.bp_window}},this_month_users.first_order_date)
    AND O.ORDER_STATE NOT IN 
        ('canceled_for_payment_error', 
        'canceled_by_fraud', 
        'canceled_by_split_error',
        'canceled_by_early_regret')
    AND O.VERTICAL_GROUP = 'RESTAURANTS'
    AND O.MICROZONE_ID IS NOT NULL
  )
 , bad_experience_score_orders as (
      select 
    distinct
      o.created_at::date as created_at,
        o.application_user_id,
      o.order_id,
      o.store_id,
      o.microzone_id,
      o.order_moment,
      coalesce(bes.bad_experience_score_adjusted,bes_fallback.bad_experience_score_adjusted) bad_experience_score
    from orders o
    left join cl_writable.STORES_BAD_EXPERIENCES_SCORE bes on 1=1
                                  AND o.store_id = bes.store_id 
                                  and bes.country = '{{params.country}}' 
                                  and bes.UPDATED_AT::DATE = o.CREATED_AT::DATE
                                  and bes.moment = o.order_moment
                                  and bes.microzone_id = o.microzone_id
                                  and bes.microzone_id != 0
    left join cl_writable.STORES_BAD_EXPERIENCES_SCORE bes_fallback on 1=1
                                  AND o.store_id = bes_fallback.store_id 
                                  and bes_fallback.country = '{{params.country}}' 
                                  and bes_fallback.UPDATED_AT::DATE = o.CREATED_AT::DATE
                                  and bes_fallback.moment = o.order_moment
                                  and bes_fallback.microzone_id = 0 
        )
    
    select 
        application_user_id,
        avg(bad_experience_score) avg_bad_experience_score
    from bad_experience_score_orders
    where 1 = 1
    group by 1
)



, closer_user_features as (

        select 
        this_month_users.application_user_id,
        first_order_date,
        {{params.country}}_users.ACQUISITION_SCORE,
        {{params.country}}_users.RETENTION_SCORE,
        {{params.country}}_users.REACTIVATION_SCORE,
        {{params.country}}_users.SEGMENT_RFM,
        {{params.country}}_users.CATEGORY,
        {{params.country}}_users.ULTRA_SCORE,
        {{params.country}}_users.TIER_RCT,
        ABS(datediff(day,first_order_date,start_date)) difference_in_days,
        row_number() over (partition by this_month_users.application_user_id order by difference_in_days asc) as ranking
        from this_month_users
        left join GLOBAL_FINANCES.{{params.country}}_APPLICATION_USERS_HIST {{params.country}}_users on this_month_users.application_user_id = {{params.country}}_users.id
        qualify ranking = 1
 
)


-- reviews made by new users of each month in the following 5 months
, reviews as (


        select 
                r.created_at::date as created_at 
                , r.order_id
                , r.score
                , '{{ params.country }}' country
        from {{ params.country }}_pg_ms_partners_growth_public.reviews r
        where not nvl(r._fivetran_deleted, false)
            and r.created_at::date between  '{{params.month}}' and DATEADD(day,-1,DATEADD(month,6,'{{params.month}}'))




)

-- Orders L365D
, orders_last_year as (
  select
    o.application_user_id,
    '{{params.month}}' :: date as first_order_month,
    count(order_id) ORDERS_L365_DAYS,
    count(distinct brand_taxo.category) as DISTINCT_CATEGORIES_BEFORE_BP
  from
    global_finances.{{ params.country }}_orders o
    inner join this_month_users on o.application_user_id = this_month_users.application_user_id -- Only this month users
    left join {{ params.country }}_writable.rests_taxonomy_brands_taxonomies_view_v2 brand_taxo on brand_taxo.brand_id = o.brand_id

  where 1=1
    and o.created_at :: date between DATEADD(day, -365, this_month_users.first_order_date) and DATEADD(day, -1, this_month_users.first_order_date)
    and o.state_type = 'FINISHED'
    and o.vertical_group = 'RESTAURANTS'
  group by
    1,
    2
)

-- First order in rappi

, first_order_in_rappi as (

  select o.application_user_id,
        datediff(month,o.created_at::date,this_month_users.first_order_date) as AGE_IN_RAPPI_IN_MONTHS
  from global_finances.{{ params.country }}_orders o
  inner join this_month_users on o.application_user_id = this_month_users.application_user_id -- Only this month users
  where o.ORDER_TYPE = 'FIRST ORDER'
)

-- Most ordered city per user.
, most_ordered_city as (
  select
    o.application_user_id,
    '{{params.month}}'::date as first_order_month,
    microzone_city.city,
    orders_last_year.ORDERS_L365_DAYS,
    orders_last_year.DISTINCT_CATEGORIES_BEFORE_BP,
    this_month_users.first_order_date,
    count(order_id) city_orders,
    row_number() over (partition by o.application_user_id order by city_orders desc) as rank_city
  from
    global_finances.{{ params.country }}_orders o
    inner join this_month_users on o.application_user_id = this_month_users.application_user_id -- Solo este mes
    left join microzone_city on microzone_city.microzone_id = o.microzone_id and microzone_city.microzone = o.microzone_name
    left join orders_last_year on orders_last_year.application_user_id = o.application_user_id
  where 1=1
    and o.created_at::date between DATEADD(day, -365, this_month_users.first_order_date)
    and this_month_users.first_order_date
    and o.state_type = 'FINISHED'
    and o.vertical_group = 'RESTAURANTS'
  group by
    1,
    2,
    3,
    4,
    5,
    6
  qualify rank_city = 1
)
-- Get the category of their first order 
, first_orders_category as (
  with aux as (
    select o.application_user_id,
    brand_taxo.category,
    o.order_id,
    first_order_date,
    o.created_at::date as created_at,
    row_number() over (partition by o.application_user_id, brand_taxo.category order by o.order_id asc) as n_order 
    from global_finances.{{params.country}}_orders o
    left join {{ params.country }}_writable.rests_taxonomy_brands_taxonomies_view_v2 brand_taxo on brand_taxo.brand_id = o.brand_id
    inner join most_ordered_city on o.application_user_id = most_ordered_city.application_user_id
    where
    1=1
    and state_type = 'FINISHED'
    and vertical_group = 'RESTAURANTS'
    qualify n_order = 1
    )

  select application_user_id,
  order_id
  from aux
  where  created_at::date between  first_order_date and DATEADD(day,{{params.bp_window}},first_order_date)
  

)

--First order in rappi category
, first_order_in_rappi_category as (
    with aux as (
    select o.application_user_id,
    mzc.microzone,
    brand_taxo.category,
    o.order_id,
    first_order_date,
    o.created_at::date as created_at,
    row_number() over (partition by o.application_user_id order by o.order_id asc) as n_order 
    from global_finances.{{params.country}}_orders o
    left join {{ params.country }}_writable.rests_taxonomy_brands_taxonomies_view_v2 brand_taxo on brand_taxo.brand_id = o.brand_id
    inner join most_ordered_city on o.application_user_id = most_ordered_city.application_user_id
    left join microzone_city mzc on mzc.microzone_id = o.microzone_id and mzc.microzone = o.microzone_name 

    where
    1=1
    and state_type = 'FINISHED'
    and vertical_group = 'RESTAURANTS'
    qualify n_order = 1
    )

  select application_user_id,
  microzone as first_microzone,
  category as first_order_in_rappi_category
  from aux
  
)
-- Orders made by new users of each month in the initial window after their first order.
-- Calculate operational and exploration variables of interest for each user.
, brand_quantity_user as (
    select 
      o.application_user_id,
      most_ordered_city.first_order_month as first_order_month,
      most_ordered_city.first_order_date as first_order_date,
      most_ordered_city.city as city_name,
      most_ordered_city.ORDERS_L365_DAYS,
      most_ordered_city.DISTINCT_CATEGORIES_BEFORE_BP,
      BES.avg_bad_experience_score,
      count(distinct case when o.state_type = 'FINISHED' then o.brand_id else null end) different_brands,
      count(case when o.state_type = 'FINISHED' then  o.order_id else null end) quant_orders,
      count(distinct case when o.state_type = 'FINISHED' and tickets.order_id is not null then tickets.order_id  else null end) tickets,
      avg(case when o.state_type = 'FINISHED' then r.score else null end) mean_score,
      sum(case when o.state_type = 'FINISHED' and r.score is not null then 1 else 0 end) quant_reviews,
      sum(case when o.state_type = 'FINISHED' and r.score is not null and r.score <= 3 then 1 else 0 end) quant_bad_reviews,
      count(distinct case when o.state_type = 'FINISHED' then brand_taxo.category else null end) quant_distinct_categories,
      sum(case when cr.order_id is not null then 1 else 0 end) user_canceled_order,
      sum(case when first_orders_category.order_id is not null then 1 else 0 end ) category_first_orders,
      sum(case when compensations.order_id is not null then compensations.amount else 0 end ) user_compensation_amount,
      sum(case when o.state_type = 'FINISHED' then o.total_value + o.tip else 0 end) user_total_value_payed
    from global_finances.{{ params.country }}_orders o
    inner join most_ordered_city on o.application_user_id = most_ordered_city.application_user_id
    left join ops_global.non_live_tickets_details_v2 tickets on o.order_id = tickets.order_id  and tickets.country = '{{params.country}}' and (tickets.level_3 in ('missing_item', 'wrong_item', 'damaged_item') or tickets.level_2 in ('Missing product','different product','Product in poor condition'))
    left join reviews r on r.order_id = o.order_id and r.country = '{{params.country}}'
    left join {{ params.country }}_writable.rests_taxonomy_brands_taxonomies_view_v2 brand_taxo on brand_taxo.brand_id = o.brand_id
    left join ops_global.cancellation_reasons cr on cr.order_id = o.order_id and cr.country = '{{params.country}}' 
    left join first_orders_category on o.order_id = first_orders_category.order_id
    left join {{ params.country }}_pg_ms_compensations_public.compensations_log compensations on compensations.order_id = o.order_id and compensations.IS_VALID_SATUS
    left join bad_experience_score bes on o.application_user_id = bes.application_user_id
    where 1=1
    and o.created_at::date between  most_ordered_city.first_order_date and DATEADD(day,{{params.bp_window}},most_ordered_city.first_order_date)
    and vertical_group = 'RESTAURANTS'
    and o.order_state not in 
    ('canceled_for_payment_error', 
    'canceled_by_fraud', 
    'canceled_by_split_error',
    'canceled_by_early_regret')
    group by 1,2,3,4,5,6,7
)

, brands_with_2_or_more_orders_initial as (
   
  with ordenes_brand as (
  select o.application_user_id,
  o.brand_id,
  count(distinct o.order_id) orders
  from global_finances.{{ params.country }}_orders o
  inner join this_month_users on o.application_user_id = this_month_users.application_user_id  
   and o.created_at::date between  this_month_users.first_order_date and DATEADD(day,{{params.bp_window}},this_month_users.first_order_date)

  group by 1,2
    )
  
  select application_user_id,
  sum(case when orders >= 2 then 1 else 0 end) as brands_with_2_orders_or_more
  from ordenes_brand
  group by 1
  
)

, ratios as (
select *
, div0(quant_orders,DIFFERENT_BRANDS) as mean_orders_per_brand
, div0(DIFFERENT_BRANDS,quant_orders) exploration_ratio
, 0 ratio
from brand_quantity_user
where quant_orders >= {{params.min_orders}}
order by different_brands desc
)

, post_orders as (
select
  o.application_user_id,
  count(distinct o.order_id) post_orders,
  count(distinct o.brand_id) post_brands,
  avg(r.score) post_mean_score,
  sum(
    case
      when r.score is null then 0
      else 1
    end
  ) post_quant_reviews
from global_finances.{{ params.country }}_orders o
inner join ratios on o.application_user_id = ratios.application_user_id
left join reviews r on r.order_id = o.order_id
where 
  o.created_at::date between DATEADD(day,{{params.bp_window}}+1,ratios.first_order_date) and DATEADD(day,1+{{params.ep_window}}+{{params.bp_window}},ratios.first_order_date)
  and state_type = 'FINISHED'
    and vertical_group = 'RESTAURANTS'
  group by 1
)

, first_orders as (
  with aux as (
    select
    o.application_user_id,
    o.store_id,
    o.brand_id,
    o.order_id,
    o.created_at::date as created_at,
    this_month_users.first_order_date,
    row_number() over (partition by o.application_user_id,o.brand_id,o.store_id order by o.created_at asc) as n_order
    from global_finances.{{params.country}}_orders o
    inner join this_month_users on this_month_users.application_user_id = o.application_user_id
    where 1=1
    and count_to_gmv
    and vertical_group = 'RESTAURANTS' 
    and o.state_type = 'FINISHED'
    qualify n_order=1
  )

  select application_user_id,
          count(order_id) FIRST_ORDERS_QUANTITY
  from aux 
  where created_at between  first_order_date and DATEADD(day,{{params.bp_window}},first_order_date)
  group by 1
  

)


select '{{ params.country }}' as country,
 {{params.bp_window}} as days_initial_time_window,
ratios.application_user_id,
0 as new_user, -- 0 for old users
ratios.city_name,
ratios.first_order_month,
ratios.different_brands,
ratios.quant_orders,
ratios.tickets,
ratios.mean_score,
ratios.quant_reviews,
-1 as bad_exp_orders,
ratios.quant_distinct_categories,
ratios.user_canceled_order,
ratios.category_first_orders,
DIV0(ratios.user_canceled_order,ratios.quant_orders) user_cancel_rate,
DIV0(ratios.category_first_orders,ratios.quant_orders) first_order_category_rate,
DIV0(ratios.quant_bad_reviews,ratios.quant_reviews) bad_reviews_rate,
ratios.ORDERS_L365_DAYS,
DIV0(ratios.user_compensation_amount,ratios.user_total_value_payed) compensation_rate,
zeroifnull(first_orders.FIRST_ORDERS_QUANTITY) FIRST_ORDERS_QUANTITY,
ZEROIFNULL(post_orders.post_orders) post_orders,
ZEROIFNULL(post_orders.post_brands) post_brands,
ZEROIFNULL(post_orders.post_mean_score) post_mean_score,
ZEROIFNULL(post_orders.post_quant_reviews) post_quant_reviews,
first_order_in_rappi_category.first_order_in_rappi_category as first_brand_category,
brands_2_orders.brands_with_2_orders_or_more,
app_users.ACQUISITION_SCORE,
app_users.RETENTION_SCORE,
app_users.REACTIVATION_SCORE,
app_users.SEGMENT_RFM,
app_users.CATEGORY,
app_users.ULTRA_SCORE,
app_users.TIER_RCT,
first_order_in_rappi_category.first_microzone,
first_order_in_rappi.AGE_IN_RAPPI_IN_MONTHS,
ratios.AVG_BAD_EXPERIENCE_SCORE,
ratios.DISTINCT_CATEGORIES_BEFORE_BP
from ratios
left join post_orders on post_orders.application_user_id = ratios.application_user_id
inner join brands_with_2_or_more_orders_initial brands_2_orders on brands_2_orders.application_user_id = ratios.application_user_id
left join closer_user_features app_users on ratios.application_user_id = app_users.APPLICATION_USER_ID
left join first_orders on first_orders.application_user_id = ratios.application_user_id
left join first_order_in_rappi_category on first_order_in_rappi_category.application_user_id = ratios.application_user_id
left join first_order_in_rappi on first_order_in_rappi.application_user_id = ratios.application_user_id
;

