delete from {{params.schema}}.analytics_retention_vs_exploration
where country  = '{{params.country}}'
and new_user = 0
and DAYS_INITIAL_TIME_WINDOW = {{params.bp_window}};