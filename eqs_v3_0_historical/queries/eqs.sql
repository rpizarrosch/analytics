begin;

--use warehouse tools_load;

CREATE OR REPLACE TABLE {{ params.schema }}.{{ params.country }}_eqs_v{{ params.version }}_{{params.day|replace("-","_")}}  AS (
  with tbl_scores as (
    select a.*, b.min_individual_fs as inherited_fs
    from {{ params.schema }}.{{ params.country }}_individual_fs_v{{ params.version }}_{{params.day|replace("-","_")}}  a 
      left join {{ params.schema }}.{{ params.country }}_inherited_fs_v{{ params.version }}_{{params.day|replace("-","_")}}  b on a.store_id = b.store_id 
  )
  select a.*
        , '{{ts}}' as created_at 
        , (case when state_user_distinct = 0 and inherited_fs is null then individual_fs
                when state_user_distinct = 0 and inherited_fs is not null then inherited_fs
                when state_user_distinct = 1 and inherited_fs is null then individual_fs
                when state_user_distinct = 1 and inherited_fs is not null then greatest(-abs((cant_users_distinct / 100) * individual_fs - (1 - (cant_users_distinct / 100)) * inherited_fs), -170)
                when state_user_distinct = 2 then individual_fs
          else -85 end) as eqs
  from tbl_scores a
);