"""Daily computing of experience variable."""

from airflow import DAG
from dags.resto_ds import settings
from datetime import datetime
import os
from plugins.operators.snowflake_operator import MultiStatementSnowflakeOperator
from airflow.operators.dummy_operator import DummyOperator
import pandas as pd

default_args = settings.get_default_args(owners=["lorena.prodriguez@rappi.com", "kervy.rivas@rappi.com"])

queries_base_path = os.path.join(os.path.dirname(__file__), "queries")
day_range = pd.date_range(start="2020-01-01", end="2020-12-31", freq="D")

if os.environ["AIRFLOW_ENVIRONMENT"] == "production":
    countries = ["BR", "CO", "AR", "PE", "CL", "MX", "UY", "EC"]
    schema = "events_home_restaurants"
else:
    schema = "pe_writable"
    countries = ["CO", "MX"]

version = "3_0"

with DAG(
    f"historical_experience_quality_v{version}",
    max_active_runs=1,
    default_args=default_args,
    schedule_interval="0 21 * * *",
    start_date=datetime(2022, 5, 1),
    catchup=False,
    template_searchpath=queries_base_path,
) as dag:

    countries_tasks = list()
    # Checks that the quantity of stores per country at the dag execution date  does not
    # change more than 20% from the mean quantity of stores by country
    # in the past (all times excluding the dag execution date)

    start_op = DummyOperator(task_id="start_task", dag=dag)
    end_op = DummyOperator(task_id="end_task", dag=dag)
    for country in countries:

        for day in day_range:

            eqs_task = MultiStatementSnowflakeOperator(
                task_id=f"{str(day)[:10]}_{country}_calculate_eqs_{version}",
                sql="eqs_total.sql",
                params={"schema": schema, "version": version, "country": country, "day": str(day)[:10]},
            )

            start_op >> eqs_task
