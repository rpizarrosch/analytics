from utils.s3_connection import S3_Connection

s3 = S3_Connection()


df_eqs = s3.get_dataframe(
    bucket="restaurants-tmp-prod",
    key="user-recommendations/mx/index/favorites_eqs_v4_0_a80_dinner.csv",
    separator=":",
    header=None,
    skiprows=1,
    names=['USER_ID', 'RECOMMENDATIONS']
)

df_eqs['RECOMMENDATIONS'] = df_eqs['RECOMMENDATIONS'].apply(str)
df_eqs['N_RECS'] = df_eqs['RECOMMENDATIONS'].apply(lambda x: len(x.split(',')))

print("EQS v4 MX Dinner Describe")
print(df_eqs.describe())

df_bes = s3.get_dataframe(
    bucket="restaurants-tmp-prod",
    key="user-recommendations/mx/index/favorites_bes_v1_0_V12_BES_DEFECTED_CANCELED_ONLY_3_FO_dinner.csv",
    separator=":",
    header=None,
    skiprows=1,
    names=['USER_ID', 'RECOMMENDATIONS']
)

df_bes['RECOMMENDATIONS'] = df_bes['RECOMMENDATIONS'].apply(str)
df_bes['N_RECS'] = df_bes['RECOMMENDATIONS'].apply(lambda x: len(x.split(',')))

print("BES v12 MX Dinner Describe")
print(df_bes.describe())
