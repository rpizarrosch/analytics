from utils.regression_models import RegressionModel
import pandas as pd

# 1000 insertions per user
n_recs = [5000, 10000, 25000, 40000]
redis_size = [0.3, 0.613, 1.50, 2.40]

# 500 insertions per user
n_recs = [5000, 10000, 25000, 40000, 70000, 80000, 100000]
redis_size = [0.06, 0.138, 0.34455, 0.55068, 0.96294, 1.07, 1.34]

# 750 insertions per user
n_recs = [5000, 10000, 25000, 40000, 70000]
redis_size = [0.227, 0.453, 1.11, 1.77, 3.10]


df = pd.DataFrame({"n_recs": n_recs, "redis_size": redis_size})
model = RegressionModel(df, "redis_size")

fitted_model = model.fit_regression()

df_results = model.get_results_dataframe()

print(df_results)
print(df_results.loc[df_results["variable"] == "const", "betas"][0])
print(fitted_model.predict([df_results.loc[df_results["variable"] == "const", "betas"][0], 1000000]))
