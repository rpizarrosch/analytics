import redis
from utils.query_loader import QueryLoader
from pathlib import Path
import pandas as pd
import numpy as np

template_sources = str(Path(__file__).parent.resolve()) + "/templates"
queries = QueryLoader(template_sources)

countries = ["AR", "BR", "CL", "CO", "PE", "MX", "CR", "EC", "UY"]
TIME_PERIOD = "MONTH"
AMOUNT_OF_TIME_PERIOD = 3
COUNTRY = "CL"
N_ROWS = 1000001

print(COUNTRY)


df_country = queries.execute(
    "recomendations_per_user",
    params={
        "COUNTRY": COUNTRY,
        "TIME_PERIOD": TIME_PERIOD,
        "AMOUNT_OF_TIME_PERIOD": AMOUNT_OF_TIME_PERIOD,
        "N_ROWS": N_ROWS,
    },
)

print(df_country)
r = redis.from_url("redis://default:redispw@localhost:55001")
r.flushall()
print(r.info("memory")["used_memory_human"])
n_rows_tryouts = [5000, 10000, 25000, 40000, 70000, 80000, 100000, 150000, 200000, 1000000]
keys = []
values = []
memory_used = []
rows_inserted = []


for i, row in df_country.iterrows():
    brand_list = list(np.random.randint(1, 488390, 500))

    moment = int(row["MOMENT"])
    microzone_id = int(row["MICROZONE_ID"])
    user_id = int(row["USER_ID"])

    # brand_id = int(row["BRAND_ID"])

    key = f"{user_id}:{moment}:{microzone_id}"
    zip_dict = {}
    for brand_id in brand_list:
        zip_dict[int(brand_id)] = float(np.random.random(1)[0])

    r.hmset(key, zip_dict)

    if i in n_rows_tryouts:
        rows_inserted.append(r.dbsize())
        memory_used.append(r.info("memory")["used_memory_human"])
        print(i)
        print(memory_used)
