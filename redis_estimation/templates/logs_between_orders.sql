WITH between_orders as (
    SELECT 
        o.country,
        o.created_at,
        '{{ country }}_' || o.application_user_id as user_id,
        o.order_id,
        LEAD(o.created_at) OVER(PARTITION BY o.application_user_id ORDER BY o.created_at) next_order_datetime
    FROM global_finances.global_orders o
    LEFT JOIN events_home_restaurants.source_by_order ON o.country = source_by_order.country
                                                    AND o.order_id = source_by_order.order_id
    WHERE 1 = 1
        AND o.country = '{{ country }}'
        AND o.created_at >= DATEADD('day',-90,CURRENT_DATE)
        AND o.store_type = 'restaurant'
        AND o.order_state NOT IN (
                'canceled_for_payment_error'
                , 'canceled_by_fraud'
                , 'canceled_by_split_error'
            )
        AND source_by_order.source NOT IN ('GLOBAL_SEARCH','others') 
        AND source_by_order.source IS NOT NULL

)


, user_logs as (

   SELECT 
        o.country,
        o.user_id,
        o.order_id,
        o.created_at,
        o.next_order_datetime,
        COUNT(DISTINCT log_id) logs_between_orders
    FROM between_orders o
    LEFT JOIN events_home_restaurants.index_data_log ul ON o.user_id = ul.user_id 
                                                    and o.country = ul.country
                                                    and ul.created_at_utc between o.created_at and o.next_order_datetime
    WHERE 1 = 1
        AND ul.created_at_utc >= DATEADD('day',-90,CURRENT_DATE)
        AND ul.country = '{{ country }}'
        AND o.created_at >= '2022-07-16'
    GROUP BY 
        o.country,
        o.user_id,
        o.order_id,
        o.created_at,
        o.next_order_datetime

)
select 
    country,
    user_id,
    count(distinct order_id) user_orders,
    sum(logs_between_orders) user_logs,
    AVG(logs_between_orders) avg_logs_between_orders,
    STDDEV(logs_between_orders) std_logs_between_orders
from user_logs
group by
    country,
    user_id