WITH logs_user_moment as (
    SELECT 
        country,
        moment,
        user_id,
        sum(total_logs) user_moment_logs
    FROM events_home_restaurants.consolidated_prediction_space
    GROUP BY 
        country,
        moment,
        user_id
)

, combinations_user_moment as (
    SELECT 
        country,
        moment,
        user_moment_logs,
        count(*) moment_user_combinations
    FROM logs_user_moment
    GROUP BY 
        country,
        moment,
        user_moment_logs
)

, country_user_moment_combinations as (
    SELECT
        country,
        moment,
        sum(moment_user_combinations) country_combinations
    FROM combinations_user_moment
    GROUP BY 
        country,
        moment
)
SELECT 
    combinations_user_moment.country,
    combinations_user_moment.moment,
    combinations_user_moment.user_moment_logs,
    combinations_user_moment.moment_user_combinations,
    country_user_moment_combinations.country_combinations,
    moment_user_combinations/country_combinations as perc_users
FROM combinations_user_moment
LEFT JOIN country_user_moment_combinations on combinations_user_moment.country = country_user_moment_combinations.country
                                            and combinations_user_moment.moment = country_user_moment_combinations.moment
ORDER BY 
    combinations_user_moment.country,
    combinations_user_moment.moment,
    combinations_user_moment.user_moment_logs