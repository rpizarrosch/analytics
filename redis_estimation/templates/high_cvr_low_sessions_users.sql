with logs_per_user as (
    SELECT 
        country,
        SUBSTRING(user_id,4) AS user_id,
        sum(daily_logs) user_logs
    FROM events_home_restaurants.daily_prediction_space_user_microzone_moment
    WHERE 1=1
        --AND country = '{{ country }}'
        AND created_at BETWEEN DATEADD('day',-89,CURRENT_DATE) AND DATEADD('day',-1,CURRENT_DATE)
    GROUP BY 
        country,
        user_id
)

, orders_per_user as (
    SELECT 
        global_orders.country,
        global_orders.application_user_id::varchar as user_id,
        COUNT(DISTINCT global_orders.order_id) user_orders
    FROM global_finances.global_orders
    LEFT JOIN events_home_restaurants.source_by_order ON global_orders.country = source_by_order.country
                                                    AND global_orders.order_id = source_by_order.order_id
    WHERE 1=1
        --AND global_orders.country = '{{country}}'
        AND vertical_group = 'RESTAURANTS'
        AND order_state NOT IN (
                'canceled_for_payment_error'
                , 'canceled_by_fraud'
                , 'canceled_by_split_error'
            )
        AND global_orders.created_at BETWEEN DATEADD('day',-89,CURRENT_DATE) AND DATEADD('day',-1,CURRENT_DATE)
        AND source_by_order.source NOT IN ('GLOBAL_SEARCH','others') 
        AND source_by_order.source IS NOT NULL
    GROUP BY 
        global_orders.country,
        global_orders.application_user_id
)


SELECT
    orders_per_user.country,
    orders_per_user.user_id,
    user_orders,
    user_logs,
    ZEROIFNULL(user_orders/user_logs) AS cvr
FROM orders_per_user
LEFT JOIN logs_per_user ON orders_per_user.user_id = logs_per_user.user_id
                        AND orders_per_user.country = logs_per_user.country
WHERE 1=1