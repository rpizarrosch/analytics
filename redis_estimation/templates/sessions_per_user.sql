 with logs_per_user as (
    SELECT 
        country,
        SUBSTRING(user_id,4) AS user_id,
        microzone_id,
        sum(daily_logs) user_mz_logs
    FROM events_home_restaurants.daily_prediction_space_user_microzone_moment
    WHERE 1=1
        --AND country = '{{ country }}'
        AND created_at BETWEEN DATEADD('day',-89,CURRENT_DATE) AND DATEADD('day',-1,CURRENT_DATE)
    GROUP BY 
        country,
        user_id,
        microzone_id
)
, sessions as (
    select 
        SUBSTRING(s.user_id,4) user_id
        , s.microzone_id
        , s.country
        , COUNT(DISTINCT s.session_id) user_mz_sessions
    from events_home_restaurants.sessions_calculated_information s
    where s.hr_session = 1
    and s.created_at_tz::date BETWEEN DATEADD('day',-89,CURRENT_DATE) AND DATEADD('day',-1,CURRENT_DATE)
    and s.microzone_id is not null
    --AND country = '{{ country }}'
    GROUP BY 
        s.country,
        s.user_id,
        s.microzone_id
)


SELECT
    l.country,
    l.user_id,
    l.microzone_id,
    ZEROIFNULL(user_mz_sessions) user_mz_sessions,
    user_mz_logs
FROM logs_per_user l
LEFT JOIN sessions s ON s.user_id = l.user_id
                        AND s.country = l.country
                        AND s.microzone_id = l.microzone_id
WHERE 1=1
AND l.microzone_id is not null