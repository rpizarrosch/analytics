
WITH amount_combinations_with_predictions as (

    SELECT 
        country,
        count(*) n_combinations
    FROM events_home_restaurants.consolidated_prediction_space
        WHERE 1 = 1
            AND microzone_id IS NOT NULL
            AND
            (
                mz_logs >= {{ min_mz_logs }}
                OR
                (
                    moment = 'morning' and moment_logs >= {{ min_morning_logs }}
                    or
                    moment = 'noon' and moment_logs >= {{ min_noon_logs }} 
                    or
                    moment = 'afternoon' and moment_logs >= {{ min_afternoon_logs }}
                    or
                    moment = 'night' and moment_logs >= {{ min_night_logs }}
                )
            )
            AND country = '{{ country }}'
    group by 1
)

, fallback_mz_franjas as (
    with combinations as (
    SELECT 
        DISTINCT 
        country,
        microzone_id,
        moment
    FROM events_home_restaurants.consolidated_prediction_space
    )
    SELECT  
        country,
        count(*) fallback_mz_franjas_combinations
    FROM combinations
    GROUP BY
        country
)

, fallback_mz as (
    with user_moment_logs as (
        SELECT 
        DISTINCT
            country,
            user_id,
            moment,
            moment_logs as user_moment_logs
        FROM events_home_restaurants.consolidated_prediction_space
        WHERE 1=1 
            AND country = '{{ country }}'
    )
    , combinations as (
    SELECT  
        DISTINCT
        country,
        user_id,
        moment
    FROM user_moment_logs
    WHERE 1=1
        AND (       
            moment = 'morning' and user_moment_logs >= {{ min_morning_logs }}
            or
            moment = 'noon' and user_moment_logs >= {{ min_noon_logs }} 
            or
            moment = 'afternoon' and user_moment_logs >= {{ min_afternoon_logs }}
            or
            moment = 'night' and user_moment_logs >= {{ min_night_logs }}
        )
    )
    SELECT
        country,
        count(*) fallback_mz_combinations
    FROM combinations
    GROUP BY 
        country
)

SELECT 
    amount_combinations_with_predictions.country,
    amount_combinations_with_predictions.n_combinations,
    fallback_mz_franjas.fallback_mz_franjas_combinations,
    fallback_mz.fallback_mz_combinations
FROM amount_combinations_with_predictions
LEFT JOIN fallback_mz_franjas ON fallback_mz_franjas.country = amount_combinations_with_predictions.country
LEFT JOIN fallback_mz ON fallback_mz.country = amount_combinations_with_predictions.country