WITH logs_user_mz as (
    SELECT 
        country,
        microzone_id,
        user_id,
        sum(total_logs) user_mz_logs
    FROM events_home_restaurants.consolidated_prediction_space
    GROUP BY 
        country,
        microzone_id,
        user_id
)

, combinations_user_mz as (
    SELECT 
        country,
        user_mz_logs,
        count(*) mz_user_combinations
    FROM logs_user_mz
    GROUP BY 
        country,
        user_mz_logs
)

, country_user_mz_combinations as (
    SELECT
        country,
        sum(mz_user_combinations) country_combinations
    FROM combinations_user_mz
    GROUP BY 
        country
)
SELECT 
    combinations_user_mz.country,
    combinations_user_mz.user_mz_logs,
    combinations_user_mz.mz_user_combinations,
    country_user_mz_combinations.country_combinations,
    mz_user_combinations/country_combinations as perc_users
FROM combinations_user_mz
LEFT JOIN country_user_mz_combinations on combinations_user_mz.country = country_user_mz_combinations.country
ORDER BY 
    country,
    user_mz_logs