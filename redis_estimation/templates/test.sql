
{% for my_item in my_collection %}

    {% if loop.index0 == 0 %}
        ( {{my_item}},
    {% elif loop.index == loop.length %}
        {{my_item}} )
    {% else %}
        {{my_item}},
    {% endif %}

{% endfor %}
