with logs_per_user as (
    SELECT 
        country,
        SUBSTRING(user_id,4) AS user_id,
        microzone_id,
        sum(daily_logs) user_mz_logs
    FROM events_home_restaurants.daily_prediction_space_user_microzone_moment
    WHERE 1=1
        --AND country = '{{ country }}'
        AND created_at BETWEEN DATEADD('day',-89,CURRENT_DATE) AND DATEADD('day',-1,CURRENT_DATE)
    GROUP BY 
        country,
        user_id,
        microzone_id
)

, orders_per_user as (
    SELECT 
        global_orders.country,
        global_orders.application_user_id::varchar as user_id,
        global_orders.microzone_id,
        COUNT(DISTINCT global_orders.order_id) user_mz_orders
    FROM global_finances.global_orders
    LEFT JOIN events_home_restaurants.source_by_order ON global_orders.country = source_by_order.country
                                                    AND global_orders.order_id = source_by_order.order_id    
                                                    and global_orders.store_id = source_by_order.store_id

    WHERE 1=1
        --AND global_orders.country = '{{ country }}'
        AND vertical_group = 'RESTAURANTS'
        AND order_state NOT IN (
                'canceled_for_payment_error'
                , 'canceled_by_fraud'
                , 'canceled_by_split_error'
            )
        AND global_orders.created_at BETWEEN DATEADD('day',-89,CURRENT_DATE) AND DATEADD('day',-1,CURRENT_DATE)
        AND source_by_order.source NOT IN ('GLOBAL_SEARCH','others') 
        AND source_by_order.source IS NOT NULL
    GROUP BY 
        global_orders.country,
        global_orders.application_user_id,
        global_orders.microzone_id
)


SELECT
    logs_per_user.country,
    logs_per_user.user_id,
    logs_per_user.microzone_id,
    ZEROIFNULL(user_mz_orders) user_mz_orders,
    user_mz_logs
FROM logs_per_user
LEFT JOIN orders_per_user ON orders_per_user.user_id = logs_per_user.user_id
                        AND orders_per_user.country = logs_per_user.country
                        AND orders_per_user.microzone_id = logs_per_user.microzone_id
WHERE 1=1
AND logs_per_user.microzone_id is not null