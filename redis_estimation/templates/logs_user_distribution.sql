WITH total_users as (
    SELECT 
        country,
        count(distinct user_id) total_users
    FROM events_home_restaurants.consolidated_prediction_space
    GROUP BY 
        country
)

, logs_per_user as (
    SELECT 
        country,
        user_id,
        sum(total_logs) user_logs
    FROM events_home_restaurants.consolidated_prediction_space
    GROUP BY 
        country,
        user_id
)

SELECT 
    logs_per_user.country,
    logs_per_user.user_logs,
    total_users,
    count(distinct user_id) n_users,
    n_users/total_users as perc_users
FROM logs_per_user
LEFT JOIN total_users on logs_per_user.country = total_users.country
GROUP BY 
    logs_per_user.country,
    user_logs,
    total_users
ORDER BY 
    logs_per_user.country,
    user_logs