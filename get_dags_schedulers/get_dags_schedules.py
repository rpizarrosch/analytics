"""DAG to calculate bad experiences versions."""
from airflow import DAG
from dags.resto_ds import settings
from datetime import datetime
import os
from operators.snowflake_operator import MultiStatementSnowflakeOperator
from airflow.operators.python import PythonOperator
from airflow.models.dagbag import DagBag
from connections.snowflake_client import SnowflakeClient
from cron_descriptor import Options, CasingTypeEnum, ExpressionDescriptor

# DAG Setup
queries_base_path = os.path.join(os.path.dirname(__file__), "queries")

default_args = settings.get_default_args(owners=["rodrigo.pizarro@rappi.com"])


# Environment specific variables
if os.environ["AIRFLOW_ENVIRONMENT"] == "production":
    schema = "events_home_restaurants"
    stage_name = "events_home_restaurants.resto_ds_stage_prod"
    countries = ["MX", "CO", "BR", "AR", "PE"]
    interval_time = "26 month"
else:
    schema = "ar_writable"
    stage_name = "ubs_stage_test"
    countries = ["AR", "BR", "CO", "MX", "PE"]
    interval_time = "26 month"  # Since june 2020 to have 6 months prior to january of 2021.


def get_dagbag():
    dags = DagBag(load_op_links=False, read_dags_from_db=False, dag_folder="dags/resto_ds")
    print("dag_ids", dags.dag_ids)

    client = SnowflakeClient()
    options = Options()
    options.use_24hour_time_format = True
    options.throw_exception_on_parse_error = True
    options.casing_type = CasingTypeEnum.Sentence
    for dag_id in dags.dag_ids:
        dag = dags.get_dag(dag_id=dag_id)
        cron_schedule = dag.schedule_interval
        descriptor = ExpressionDescriptor(cron_schedule, options)

        try:
            readable_schedule = descriptor.get_hours_description()
            print(cron_schedule, readable_schedule)
        except Exception:
            readable_schedule = ""
        query = f"""
        INSERT INTO {schema}.DAGS_SCHEDULES (DAG_ID,CRON_SCHEDULE,READABLE_SCHEDULE)
        VALUES
        (
        {"'"+str(dag_id)+"'"},
        {"'"+str(cron_schedule)+"'"},
        {"'"+str(readable_schedule)+"'"}
        )
         """
        print(query)
        client.execute(query)
    return dags


with DAG(
    "analytics_get_schedules",
    catchup=False,
    default_args=default_args,
    schedule_interval=None,
    max_active_runs=1,
    template_searchpath=queries_base_path,
    start_date=datetime(2022, 2, 23),
) as dag:
    # List of task
    insert_user_information = MultiStatementSnowflakeOperator(
        task_id="create_table_snowflake",
        sql="create_table.sql",
        params={
            "schema": schema,
        },
    )
    test = PythonOperator(task_id="get_schedules", python_callable=get_dagbag)

    insert_user_information >> test
