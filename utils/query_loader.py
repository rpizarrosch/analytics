from jinja2 import Environment, FileSystemLoader
from pathlib import Path
from utils.get_snowflake_data import SnowflakeConnector


class QueryLoader:
    def __init__(self, template_sources=None):
        self.sources = (
            template_sources if template_sources is not None else str(Path(__file__).parent.resolve()) + "/templates"
        )
        print(self.sources)
        self.environment = Environment(loader=FileSystemLoader(self.sources))
        self.client = SnowflakeConnector()

    def __render(self, name, parameters: dict = None) -> str:
        try:
            template = self.environment.get_template(f"{name}.sql.jinja")
        except Exception:
            try:
                template = self.environment.get_template(f"{name}.sql")
            except Exception:
                pass
        statement = template.render(parameters or {})

        return statement

    def execute(self, name, print_statement=True, **kwargs):
        # Work with "parameters" or "params" kwarg:
        parameters = kwargs.pop("parameters", kwargs.pop("params", {}))
        statement = self.__render(name, parameters)
        if print_statement:
            print(f"Executing Query {statement} and params {parameters}")
        return self.client.get_pandas_dataframe_from_query(statement)
