import statsmodels.api as sm
import pandas as pd
import numpy as np


class RegressionModel:
    def __init__(self, df_regression, target_var) -> None:
        self.X = df_regression.drop(target_var, axis=1)
        self.y = df_regression[target_var]
        self.target_var = target_var
        self.df_regression = df_regression

    def fit_regression(self):
        self.df_regression.to_csv("./retention_vs_exploration/data/df_regression.csv", index=False)
        X2 = sm.add_constant(self.X)
        est = sm.OLS(self.y, X2)
        est2 = est.fit()
        self.fitted_model = est2
        return est2

    def apply_log(self, variables_to_log):
        for var in variables_to_log:
            if self.df_regression[var].min() == 0:
                print(f"Min of {var} is 0, added 1 to apply log")
                self.df_regression[var] = np.log(self.df_regression[var] + 1)
            else:
                self.df_regression[var] = np.log(self.df_regression[var])
            self.df_regression.rename(columns={var: "LOG_" + var}, inplace=True)

        if self.target_var in variables_to_log:
            self.X = self.df_regression.drop(f"LOG_{self.target_var}", axis=1)
            self.y = self.df_regression[f"LOG_{self.target_var}"]
            self.target_var = f"LOG_{self.target_var}"
        else:
            self.X = self.df_regression.drop(self.target_var, axis=1)
            self.y = self.df_regression[self.target_var]

    def get_results_dataframe(self, readability=False):

        df_regression_result = pd.DataFrame(
            {
                "variable": self.fitted_model.model.exog_names,
                "betas": self.fitted_model.params,
                "pvalues": self.fitted_model.pvalues,
            }
        )
        for i, row in df_regression_result.iterrows():
            if readability and "LOG_" in row["variable"] and "LOG_" in self.target_var:
                print(f"For {row['variable']} betas has been divided by 100 for readability")
                df_regression_result.loc[df_regression_result["variable"] == row["variable"], "betas"] = (
                    df_regression_result.loc[df_regression_result["variable"] == row["variable"], "betas"] / 100
                )
        self.df_regression_result = df_regression_result
        return df_regression_result

    def results_to_csv(self, directory, filename):
        self.df_regression_result.to_csv(f"./{directory}/regression_results/{filename}.csv", sep=";", index=False)
