import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


class QuantileAnalysis:
    def __init__(self, df, columns_to_quantile, target_column) -> None:
        self.df = df
        self.columns_to_quantile = columns_to_quantile
        self.target_column = target_column

    def quantilize_columns(self, n_quantiles):

        for column in self.columns_to_quantile:
            self.df[f"{column}_quantil"] = pd.qcut(self.df[column], n_quantiles, labels=False, duplicates="drop")
            print("Rows count per quantile: \n", self.df.groupby(f"{column}_quantil").count()[column])
            print(f"{column} Mean per quantile: \n", self.df.groupby(f"{column}_quantil").mean()[column])

        return self.df

    def get_quantile_heatmap_df(self, pair_of_columns):

        if len(pair_of_columns) != 2:
            raise Exception("Columns array must have 2 columns")

        print("Heatmap Dataframe based on: ", pair_of_columns)
        quantile_dict = {}
        df_result_columns = []
        for column in pair_of_columns:
            quantile_dict[f"{column}_unique_values"] = self.df[f"{column}_quantil"].unique()
            df_result_columns.append(column + "_quantil")
        df_result_columns.append(self.target_column)
        df_result = pd.DataFrame(columns=df_result_columns)

        for quantile_first_column in quantile_dict[f"{pair_of_columns[0]}_unique_values"]:
            for quantile_second_column in quantile_dict[f"{pair_of_columns[1]}_unique_values"]:
                df_aux = self.df[
                    (self.df[f"{pair_of_columns[0]}_quantil"] == quantile_first_column)
                    & (self.df[f"{pair_of_columns[1]}_quantil"] == quantile_second_column)
                ][[self.target_column]].mean()[[self.target_column]]
                row = pd.DataFrame.from_dict(
                    {
                        "index": [0],
                        pair_of_columns[0] + "_quantil": quantile_first_column,
                        pair_of_columns[1] + "_quantil": quantile_second_column,
                        self.target_column: df_aux.loc[self.target_column],
                    }
                )

                df_result = pd.concat([df_result, row], axis=0)
        df_result.reset_index(drop=True, inplace=True)
        df_heatmap = (
            df_result.dropna()
            .sort_values(by=[pair_of_columns[0] + "_quantil", pair_of_columns[1] + "_quantil"])
            .drop("index", axis=1)
        )
        df_heatmap_post_orders_pivot = df_heatmap.pivot(
            index=pair_of_columns[0] + "_quantil", columns=pair_of_columns[1] + "_quantil"
        )
        ind = []
        for col in df_heatmap[pair_of_columns[0] + "_quantil"].unique():
            ind.append(f"{pair_of_columns[0]}_q_" + str(col))

        cols = []
        for col in df_heatmap[pair_of_columns[1] + "_quantil"].unique():
            cols.append(f"{pair_of_columns[1]}_q_" + str(col))

        df_heatmap_post_orders_pivot = pd.DataFrame(df_heatmap_post_orders_pivot.values, index=ind, columns=cols)

        for col in df_heatmap_post_orders_pivot.columns:
            df_heatmap_post_orders_pivot[col] = df_heatmap_post_orders_pivot[col].apply(float)

        return df_heatmap_post_orders_pivot

    def plot_quantile_heatmap(self, df_heatmap, directory, png_filename, figsize=(20, 12.6)):
        plt.figure(figsize=figsize)
        sns.heatmap(df_heatmap, cmap=sns.light_palette("seagreen", as_cmap=True), annot=True)
        plt.tight_layout()
        plt.savefig(f"./{directory}/plots/{png_filename}.png", dpi=300)
        plt.show()
