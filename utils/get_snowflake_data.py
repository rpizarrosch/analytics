from dotenv import dotenv_values
import snowflake.connector
import snowflake.connector.errors
import pandas as pd

config_values = dotenv_values(".env_dev")

PASSWORD = config_values["SNOWFLAKE_PASSWORD"]
USER = config_values["SNOWFLAKE_USER"]
ACCOUNT = config_values["SNOWFLAKE_ACCOUNT"]
WAREHOUSE = config_values["SNOWFLAKE_WAREHOUSE"]
DATABASE = config_values["SNOWFLAKE_DATABASE"]
SCHEMA = config_values["SNOWFLAKE_SCHEMA"]
ROLE = config_values["SNOWFLAKE_ROLE"]


class SnowflakeConnector:
    def __init__(self) -> None:
        self.password = PASSWORD
        self.user = USER
        self.account = ACCOUNT
        self.warehouse = WAREHOUSE
        self.database = DATABASE
        self.schema = SCHEMA
        self.role = ROLE

        self.con = snowflake.connector.connect(
            user=self.user,
            url=f"{self.account}.snowflakecomputing.com",
            account=self.account,
            password=self.password,
            warehouse=self.warehouse,
            database=self.database,
            schema=self.schema,
            role=self.role,
        )

    def __get_table_columns(self, result_metadata):
        cols = []
        for row in result_metadata:
            cols.append(row.name)
        return cols

    def __execute_query(self, query):

        cur = self.con.cursor()
        try:
            result = cur.execute(query)
        except snowflake.connector.Error as e:
            print("mensaje", e.msg)
            raise e

        return result

    def __get_columns_amount_nulls(self, df):
        nulls = df.isna().sum()
        quant = nulls / len(df)

        df_nulls = pd.DataFrame({"COLUMN": nulls.index, "AMOUNT_OF_NULLS": nulls, "PERC_OF_NULLS": quant})
        df_nulls = df_nulls[df_nulls["AMOUNT_OF_NULLS"] > 0].reset_index(drop=True)
        df_nulls.sort_values(by="PERC_OF_NULLS", ascending=False, inplace=True)
        print("Amount of Nulls:\n", df_nulls)
        return df_nulls

    def get_pandas_dataframe_from_query(self, query, fillna_0=False, get_null_df=False):
        result = self.__execute_query(query)
        result_list = result.fetchall()
        cols = self.__get_table_columns(result.description)
        df = pd.DataFrame(result_list, columns=cols)

        if fillna_0:
            df.fillna(0, inplace=True)
        if get_null_df:
            df_nulls = self.__get_columns_amount_nulls(df)
            return df, df_nulls
        else:
            return df
