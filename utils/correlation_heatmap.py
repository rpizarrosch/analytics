import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


def correlation_heatmap(dataframe, visible_columns, color_palette=sns.diverging_palette(20, 145), figsize=(12.4, 10)):
    aux_cols = {}
    for col in visible_columns:
        aux_cols[col] = dataframe[col]
        dataframe.drop(col, axis=1, inplace=True)
        dataframe[col] = aux_cols[col]
    del aux_cols
    corr = dataframe.corr()
    mask = np.zeros_like(corr)
    mask[:, :] = True

    mask[dataframe.columns.get_loc(visible_columns[0]) :, : dataframe.shape[1]] = False

    with sns.axes_style("white"):

        f, ax = plt.subplots(figsize=figsize)
        ax = sns.heatmap(corr, mask=mask, cmap=color_palette, vmin=-1, vmax=1, annot=True)
        ax.set_title(f"Correlation Map - Initial windows = {30} Days")
        plt.tight_layout()
        plt.savefig(f"./retention_vs_exploration/plots/correlation_map_initial_windows_{30}_days.png", dpi=300)
