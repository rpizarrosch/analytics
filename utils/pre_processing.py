from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

from warnings import simplefilter

simplefilter(action="ignore", category=pd.errors.PerformanceWarning)


def dummies_and_concat(df, categorical_columns, drop_categoricals):

    categorical_for_dummies = list(set(categorical_columns).symmetric_difference(drop_categoricals))

    df_categorical_dummies = pd.get_dummies(df, columns=categorical_for_dummies).drop(drop_categoricals, axis=1)

    return df_categorical_dummies


def dataframe_scaling(df, drop_columns, scaler=MinMaxScaler()):

    scaler.fit(df.drop(drop_columns, axis=1))

    df_scaled = pd.DataFrame(
        scaler.transform(df.drop(drop_columns, axis=1)), columns=df.drop(drop_columns, axis=1).columns
    )

    return df_scaled


def get_nan_by_columns(df):
    result = df.isna().sum()
    print(result)
    return result


def boolean_on_nan_columns(df, df_nulls, fillna_0=False):
    for null_col in df_nulls["COLUMN"]:
        df.loc[:, f"{null_col}_is_null"] = 0
        df.loc[df[null_col].isnull(), f"{null_col}_is_null"] = 1
    if fillna_0:
        df.fillna(0, inplace=True)
    return df


def boolean_on_non_valid_values_columns(df, columns, not_valid_value, fillna_0=False):
    for null_col in columns:
        df.loc[:, f"{null_col}_is_not_valid"] = 0
        df.loc[df[null_col] == not_valid_value, f"{null_col}_is_not_valid"] = 1
    if fillna_0:
        df.fillna(0, inplace=True)
    return df


def get_dummies_by_rank_based_in_quantile(df, columns_to_dummie, columns_to_count, quantile, duplicates="raise"):
    for column, count_column in zip(columns_to_dummie, columns_to_count):
        df_group = df.groupby(column).count()[[count_column]].reset_index()
        quantiles = pd.qcut(df_group[count_column], q=quantile, labels=False, duplicates=duplicates)
        df_group[f"{column}_RANK"] = quantiles
        df = df.merge(df_group[[column, f"{column}_RANK"]], how="left", on=column)
        df.fillna({f"{column}_RANK": "OTHER"}, inplace=True)
    return df


def delete_outliers_by_percentiles(df, columns_with_outliers, percentile):

    for column in columns_with_outliers:
        df = df[df[column] <= df[column].quantile(percentile)]

    return df


def filter_columns_higher_than_zero(df, columns_to_filter):

    for column in columns_to_filter:
        df = df[df[column] > 0]

    return df


def apply_log(df, variables_to_log):
    for var in variables_to_log:
        if df[var].min() == 0:
            print(f"Min of {var} is 0, added 1 to apply log")
            df.loc[:, (var)] = np.log(df[var] + 1)
        else:
            df.loc[:, (var)] = np.log(df[var])
        df.rename(columns={var: "LOG_" + var}, inplace=True)

    return df
