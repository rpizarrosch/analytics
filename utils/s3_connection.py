from io import BytesIO
import os

import boto3
import pandas as pd

from dotenv import dotenv_values

config_values = dotenv_values(".env_dev")

AWS_ACCESS_KEY_ID = config_values["AWS_ACCESS_KEY_ID"]
AWS_SECRET_ACCESS_KEY = config_values["AWS_SECRET_ACCESS_KEY"]
AWS_SESSION_TOKEN = config_values["AWS_SESSION_TOKEN"]


class S3_Connection:
    def __init__(self) -> None:
        self.client = boto3.client(
            "s3",
            aws_access_key_id=AWS_ACCESS_KEY_ID,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            aws_session_token=AWS_SESSION_TOKEN,
        )

        self.bucket_list = self.client.list_buckets()

    def get_dataframe(
        self, bucket, key, header='infer', separator=",", on_bad_lines="warn",names = [], skiprows = 0
    ) -> pd.DataFrame:
        obj = self.client.get_object(Bucket=bucket, Key=key)
        status = obj.get("ResponseMetadata", {}).get("HTTPStatusCode")

        if status == 200:
            print(f"Successful S3 get_object response. Status - {status}")
            if len(names) > 0:
                df = pd.read_csv(
                    obj.get("Body"), sep=separator, on_bad_lines=on_bad_lines, header=header, names=names, skiprows=skiprows
                )
            else:
                df = pd.read_csv(
                    obj.get("Body"), sep=separator, on_bad_lines=on_bad_lines, header=header, skiprows=skiprows
                )
            return df
        else:
            print(f"Unsuccessful S3 get_object response. Status - {status}")

