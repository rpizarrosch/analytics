### How do I get set up? ###
1. Create a new virtual environment to keep things tidy. In this case I'm using conda
   
        $ conda create --name env_name

2. Activate the environment
   
        $ conda activate env_name

3. Run the following command on shell to install all the dependencies used in this repository

         $ pip install -r requirements.txt

#### To Query on Snowflake
Add file called .env_dev where you should have your credentials in the following format:

    SNOWFLAKE_PASSWORD = 'password'
    SNOWFLAKE_USER = 'user@rappi.com'
    SNOWFLAKE_ACCOUNT = 'hg51401'
    SNOWFLAKE_WAREHOUSE = 'WAREHOUSE'
    SNOWFLAKE_DATABASE = 'FIVETRAN'
    SNOWFLAKE_SCHEMA = 'PUBLIC'
    SNOWFLAKE_ROLE = 'ROLE'

### To use S3 Client
Add to the .env_dev file credentials in the following format:

    AWS_ACCESS_KEY_ID = "YOUR_AWS_ACCESS_KEY_ID"
    AWS_SECRET_ACCESS_KEY = "YOUR_AWS_SECRET_ACCESS_KEY"
    AWS_SESSION_TOKEN = "YOUR_AWS_SESSION_TOKEN"
